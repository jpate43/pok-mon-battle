# **Pokémon Battle** #

* This is a Pokémon battle game in which two players can battle each other through a multi-threaded server hosted on a Unix machine.
* This repository contains the source code for the clients and server, and a release folder containing an executable file for the game which will only function as a demo for the GUI.
* This game was created by Matthew Chiborak, Alex Lee Chan, Jugal Patel, and Alexander Smart.
* This is created purely for educational purposes.

### Disclaimer ###
Pokémon © 2002-2016 Pokémon. © 1995-2016 Nintendo/Creatures Inc./GAME FREAK inc. TM, ® and Pokémon character names are trademarks of Nintendo.
No copyright or trademark infringement is intended in using Pokémon content in this project.